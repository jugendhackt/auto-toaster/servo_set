# servo_set

This repository contains some scripts for controlling raspberry pi servo motors.

Help on package servo_set:

	NAME
		servo_set

	PACKAGE CONTENTS


	CLASSES
		builtins.object
			servo_set
		
		class servo_set(builtins.object)
		|  servo_set(pins)
		|  
		|  Class for controling multiple servo motors on a raspberry pi
		|  
		|  Methods defined here:
		|  
		|  __init__(self, pins)
		|      Initialize self.  See help(type(self)) for accurate signature.
		|  
		|  get_position(self, pin=None)
		|      Returning the position of the given or last used servo
		|  
		|  move(self, amount, pin=None)
		|      Moving a servo by a given relative amount
		|  
		|  reset_servo(self, pin=None)
		|      Reset a servo to its initial state (0°==1000)
		|  
		|  set_position(self, position, pin=None)
		|      Moving a servo to an absolute position. Defaultly using last active pin.
		|  
		|  stop(self)
		|      Stop all servos and disconnect from pigpio
		|  
		|  ----------------------------------------------------------------------
		|  Data descriptors defined here:
		|  
		|  __dict__
		|      dictionary for instance variables (if defined)
		|  
		|  __weakref__
		|      list of weak references to the object (if defined)
		|  
		|  ----------------------------------------------------------------------
		|  Data and other attributes defined here:
		|  
		|  current_pin = 0

	DATA
		MAX_WIDTH = 2000
		MIN_WIDTH = 1000
		NUM_GPIO = 32
		pi = <pipio.pi host=localhost port=8888>
		step = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, .....
		used = [False, False, False, False, False, False, False, False, False,...
		width = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ....

	FILE
		__init__.py
#!/usr/bin/env python3
import sys
import time
import random
import pigpio

NUM_GPIO = 32

MIN_WIDTH = 1000
MAX_WIDTH = 2000

step = [0]*NUM_GPIO
width = [0]*NUM_GPIO
used = [False]*NUM_GPIO

pi = pigpio.pi()


class servo_set():
    """Class for controling multiple servo motors on a raspberry pi"""
    current_pin = 0

    def __init__(self, pins):
        self.pins = pins
        self.current_pin = pins[0]
        if not pi.connected:
            exit()

    def reset_servo(self, pin=None):
        """Reset a servo to its initial state (0°==1000)"""
        if pin is None:
            pin = self.current_pin
        self.current_pin = pin
        pi.set_servo_pulsewidth(pin, 1500)

    def get_position(self, pin=None):
        """Returning the position of the given or last used servo"""
        if pin is None:
            pin = self.current_pin
        self.current_pin = pin
        return pi.get_servo_pulsewidth(pin)

    def set_position(self, position, pin=None):
        """Moving a servo to an absolute position. Defaultly using last active pin."""
        if pin is None:
            pin = self.current_pin
        if (position < 500 or position > 2500) and position != 0:
            print("Position out of range. Leaving pin unchanged.")
            return
        self.current_pin = pin
        amount = position-self.get_position()
        self.move(amount)

    def move(self, amount, pin=None):
        """Moving a servo by a given relative amount"""
        if pin is None:
            pin = self.current_pin
        current_position = self.get_position(pin)
        end_position = current_position+amount

        if end_position < 500 or end_position > 2500:
            print("Position out of range.")
            return

        while (end_position-current_position >= 0 and end_position-current_position > 10) or (current_position-end_position >= 0 and current_position-end_position > 10):
            if end_position-current_position > 10 and end_position-current_position < -10:
                pi.set_servo_pulsewidth(pin, end_position)
            else:
                if end_position-current_position < 0:
                    pi.set_servo_pulsewidth(pin, current_position-3)
                else:
                    pi.set_servo_pulsewidth(pin, current_position+3)
            current_position = self.get_position()
            time.sleep(.01)
        print("Moved successfully.")

    def stop(self):
        """Stop all servos and disconnect from pigpio"""
        for i in self.pins:
            pi.set_servo_pulsewidth(i, 0)
        pi.stop()
